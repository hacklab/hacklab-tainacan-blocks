import React from "react";
import ReactDOM from "react-dom";
import { App } from "./App";

const blocks = document.querySelectorAll(".hacklab-tainacan-collection-items");
blocks.forEach((block) => {
	ReactDOM.render(<App data-building={ JSON.parse(block.getAttribute('data-building')) } />, block);
})
