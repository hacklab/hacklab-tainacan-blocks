<?php
namespace hacklabTainacanBlocks;

/**
 * Tainacan Collection Items render_callback
 */
function tainacan_collection_items_callback( $attributes ) {
	$data_building = [
		'blockTitle'       => isset( $attributes['blockTitle'] ) ? $attributes['blockTitle'] : '',
		'blockSubTitle'    => isset( $attributes['blockSubTitle'] ) ? $attributes['blockSubTitle'] : '',
		'blockDescription' => isset( $attributes['blockDescription'] ) ? $attributes['blockDescription'] : ''
	];

	return '<div class="hacklab-tainacan-collection-items" data-building="' . htmlentities( json_encode( $data_building ) ) . '"></div>';
}

/**
 * Load frontend scripts
 */
function tainacan_load_scripts() {
	wp_enqueue_script(
		"hacklab-tainacan-collection-items-frontend",
		HACKLAB_TAINACAN_BLOCKS_PATH_URL . "build/js/tainacan-collection-items-frontend/tainacan-collection-items-frontend.js",
		[ 'wp-block-editor', 'wp-blocks', 'wp-element', 'wp-i18n', 'wp-polyfill', 'wp-api-fetch' ],
		"0.1.0",
		true
	);

	wp_localize_script( 'hacklab-tainacan-collection-items-frontend', 'tainacanCollectionItemsBlock', localize_data_block() );
}

add_action( 'wp_enqueue_scripts', 'hacklabTainacanBlocks\\tainacan_load_scripts' );

/**
 * Load admin scripts
 */
function tainacan_load_scripts_admin() {

	$local_terms = get_terms( [
		'taxonomy' => 'local',
		'fields'   => 'id=>name'
	] );

	$collection_id = get_collection_id( 'acervo' );

	wp_localize_script( 'hacklab-tainacan-blocks-tainacan-collection-items-editor-script', 'tainacanCollectionItemsBlock', localize_data_block() );
}

add_action( 'admin_enqueue_scripts', 'hacklabTainacanBlocks\\tainacan_load_scripts_admin' );

/**
 * Function to create all data localized of the block
 */
function localize_data_block() {
	$local_terms = get_terms( [
		'taxonomy' => 'local',
		'fields'   => 'id=>name'
	] );

	$collection_id = get_collection_id( 'acervo' );

	return [
		'localTerms'              => ( $local_terms && ! is_wp_error( $local_terms ) ) ? $local_terms : [],
		'tainacanCollectionItems' => $collection_id,
		'tainacanMetadataLocal'   => get_metadata_id( 'acervo', 'Unidade' ),
		'i18n' => [
			'allUnits'    => __( 'Ver todas as unidades', 'hacklab-tainacan-blocks' ),
			'description' => __( 'Descrição', 'hacklab-tainacan-blocks' ),
			'loading'     => __( 'Carregando...', 'hacklab-tainacan-blocks' ),
			'noGallery'   => __( 'Nenhuma obra para exibir.', 'hacklab-tainacan-blocks' ),
			'remove'      => __( 'Remover', 'hacklab-tainacan-blocks' ),
			'subtitle'    => __( 'Subtítulo', 'hacklab-tainacan-blocks' ),
			'title'       => __( 'Título', 'hacklab-tainacan-blocks' )
		]
	];
}

/**
 *
 * Filtra a consulta de busca do post type "unidades" com base em um meta_key e meta_value especificado.
 *
 * @param array $args      Os argumentos padrão da consulta para a post type "unidades".
 * @param object $request  O objeto da requisição REST API.
 * @example                /wp-json/wp/v2/unidades?meta_key=cidade&meta_value=São+Paulo
 * @return array           Os argumentos atualizados com a condição de busca personalizada adicionada.
 *
 */
function filter_unidades_by_field( $args, $request ) {

	$query_params = $request->get_query_params();

	if ( isset( $query_params['meta_key'] ) && isset( $query_params['meta_value'] ) ) {

		$meta_value = explode( ',', $query_params['meta_value'] );
		$meta_value = array_map( 'sanitize_text_field', $meta_value );

		if ( isset( $args['meta_query'] ) ) {
			$args['meta_query']['relation'] = 'AND';

			$new_meta_query = [
				[
					'key'   => sanitize_text_field( $query_params['meta_key'] ),
					'value' => $meta_value
				]
			];

			$args['meta_query'][] = $new_meta_query;
		} else {
			$args['meta_query'] = [
				[
					'key'   => sanitize_text_field($query_params['meta_key'] ),
					'value' => $meta_value
				]
			];
		}
	}

	return $args;
}

add_filter( 'rest_unidade_query', 'hacklabTainacanBlocks\\filter_unidades_by_field', 99, 2 );

/**
 * Adiciona um metadado personalizado à resposta da REST API do post type "unidade".
 *
 * @see https://developer.wordpress.org/reference/hooks/rest_api_init/
 */
add_action( 'rest_api_init', 'hacklabTainacanBlocks\\add_metadata_to_unidade_response' );

function add_metadata_to_unidade_response() {
	register_rest_field(
		'unidade',
		'relationship',
		[
			'get_callback' => function( $unidade ) {
				return get_post_meta( $unidade['id'], 'relationship', true );
			},
		]
	);
}

function get_collection_id( $title ) {
	$output = false;

	$repository = \Tainacan\Repositories\Collections::get_instance();

	$instance = $repository->fetch_one( ['s' => esc_attr( $title ) ] );

	if ( $instance ) {
		$output = $instance->get_id();
	}

	return $output;
}

function get_metadata_id( $collection_slug, $name ) {

	$repoCollections = \Tainacan\Repositories\Collections::get_instance();
	$query = $repoCollections->fetch( [ 'pagename' => $collection_slug ] );
	if ( isset( $query->posts[0] ) ) {
		$first_post = $query->posts[0];
		$collection = new \Tainacan\Entities\Collection( $first_post->ID );
		$repoMetadata = \Tainacan\Repositories\Metadata::get_instance();
		$query_metadata = $repoMetadata->fetch_by_collection( $collection, [ 'title' => $name ] );
		
		if ( ! empty( $query_metadata ) ) {
			$metadata = $query_metadata[0];
	
			if ( $metadata ) {
				return $metadata->WP_Post->ID;
			}
		}
	}

	return false;

}
