# Copyright (C) 2022 The WordPress Contributors
# This file is distributed under the GPL-2.0-or-later.
msgid ""
msgstr ""
"Project-Id-Version: hacklab-blocks 0.1.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/hacklab-blocks\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2022-05-23T18:33:39+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.6.0\n"
"X-Domain: hacklab-blocks\n"

#. Plugin Name of the plugin
msgid "hacklab-blocks"
msgstr ""

#. Description of the plugin
msgid "blocos para utilização em diversos projetos."
msgstr ""

#. Author of the plugin
msgid "The WordPress Contributors"
msgstr ""

#: blocks/geo-information/geo-information.php:152
#: blocks/geo-information/components/Card.js:39
#: build/js/geo-information-front/geo-information-front.js:87
#: build/js/geo-information-front/geo-information-front.js:54
msgid "City: "
msgstr ""

#: blocks/geo-information/geo-information.php:153
#: blocks/geo-information/components/List.js:455
#: build/js/geo-information-front/geo-information-front.js:578
#: build/js/geo-information-front/geo-information-front.js:612
msgid "Filter locations or click on a map marker"
msgstr ""

#: blocks/geo-information/geo-information.php:154
#: blocks/geo-information/components/List.js:494
#: build/js/geo-information-front/geo-information-front.js:608
#: build/js/geo-information-front/geo-information-front.js:651
msgid "Filter"
msgstr ""

#: blocks/geo-information/geo-information.php:155
#: blocks/geo-information/components/Card.js:43
#: build/js/geo-information-front/geo-information-front.js:90
#: build/js/geo-information-front/geo-information-front.js:58
msgid "Read More"
msgstr ""

#: blocks/geo-information/geo-information.php:156
#: blocks/geo-information/components/List.js:483
#: build/js/geo-information-front/geo-information-front.js:603
#: build/js/geo-information-front/geo-information-front.js:640
msgid "Select a City"
msgstr ""

#: blocks/geo-information/geo-information.php:157
#: blocks/geo-information/components/List.js:468
#: build/js/geo-information-front/geo-information-front.js:590
#: build/js/geo-information-front/geo-information-front.js:625
msgid "Select a State"
msgstr ""

#: blocks/geo-information/geo-information.php:158
#: blocks/geo-information/components/Card.js:35
#: build/js/geo-information-front/geo-information-front.js:87
#: build/js/geo-information-front/geo-information-front.js:50
msgid "State: "
msgstr ""

#: blocks/geo-information/geo-information.php:159
#: blocks/geo-information/components/Card.js:31
#: build/js/geo-information-front/geo-information-front.js:87
#: build/js/geo-information-front/geo-information-front.js:46
msgid "Testimonial: "
msgstr ""

#: blocks/geo-information/components/List.js:503
#: build/js/geo-information-front/geo-information-front.js:628
#: build/js/geo-information-front/geo-information-front.js:660
msgid "No testimonials found"
msgstr ""

#: blocks/custom-image-gallery-block/block.json
msgctxt "block title"
msgid "Galeria de imagem customizada"
msgstr ""

#: blocks/custom-image-gallery-block/block.json
msgctxt "block description"
msgid "Galeria interativa com formatos diferentes de exibição."
msgstr ""

#: blocks/custom-image-gallery-block/block.json
msgctxt "block keyword"
msgid "image"
msgstr ""

#: blocks/custom-image-gallery-block/block.json
msgctxt "block keyword"
msgid "imagem"
msgstr ""

#: blocks/custom-image-gallery-block/block.json
msgctxt "block keyword"
msgid "gallery"
msgstr ""

#: blocks/custom-image-gallery-block/block.json
msgctxt "block keyword"
msgid "galeria"
msgstr ""

#: blocks/featured-color/block.json
msgctxt "block title"
msgid "Cor de destaque"
msgstr ""

#: blocks/featured-color/block.json
msgctxt "block description"
msgid "Exibe a cor de destaque do post (a partir do metadado 'featured_color')."
msgstr ""

#: blocks/featured-color/block.json
msgctxt "block keyword"
msgid "color"
msgstr ""

#: blocks/featured-color/block.json
#: blocks/partners/block.json
msgctxt "block keyword"
msgid "post"
msgstr ""

#: blocks/featured-color/block.json
msgctxt "block keyword"
msgid "destaque"
msgstr ""

#: blocks/geo-information/block.json
msgctxt "block title"
msgid "Geo information"
msgstr ""

#: blocks/geo-information/block.json
msgctxt "block description"
msgid "Bloco para geolocalização de informações"
msgstr ""

#: blocks/partners/block.json
msgctxt "block title"
msgid "Parceiro"
msgstr ""

#: blocks/partners/block.json
msgctxt "block description"
msgid "Exibe o nome do parceiro relacionado ao post."
msgstr ""

#: blocks/partners/block.json
msgctxt "block keyword"
msgid "partner"
msgstr ""

#: blocks/partners/block.json
msgctxt "block keyword"
msgid "acervo"
msgstr ""

#: blocks/partners/block.json
msgctxt "block keyword"
msgid "parceiros"
msgstr ""

#: blocks/sample-block/block.json
msgctxt "block title"
msgid "Bloco de exemplo"
msgstr ""

#: blocks/sample-block/block.json
msgctxt "block description"
msgid "Um bloco de exemplo para orgabnizar compilação de assets."
msgstr ""

#: blocks/video-playlist/block.json
msgctxt "block title"
msgid "Vídeo playlist"
msgstr ""

#: blocks/video-playlist/block.json
msgctxt "block description"
msgid "Exibe vídeos de um canal ou playlist do youtube."
msgstr ""

#: blocks/video-playlist/block.json
msgctxt "block keyword"
msgid "playlist"
msgstr ""

#: blocks/video-playlist/block.json
msgctxt "block keyword"
msgid "video"
msgstr ""

#: blocks/video-playlist/block.json
msgctxt "block keyword"
msgid "youtube"
msgstr ""
