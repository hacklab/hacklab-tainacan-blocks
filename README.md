# Hacklab Tainacan Blocks

## Ativar o watcher do plugin para desenvolvimento

Na raiz do repositório execute

`./dev-scripts/watcher-plugin.sh`


## Compilar o plugin

Na raiz do repositório execute

`./dev-scripts/compila-plugin.sh`


## Gerar o zip do plugin

Na raiz do repositório execute, ele vai salvar o zip dentro da pasta /zip do plugin

`./dev-scripts/zip.sh`
