import { useBlockProps, RichText } from '@wordpress/block-editor';
import { __ } from '@wordpress/i18n';

import Terms from './src/components/Terms';
import Gallery from './src/components/Gallery';
import { Button } from '@wordpress/components';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @param {Object}   props               Properties passed to the function.
 * @param {Object}   props.attributes    Available block attributes.
 * @param {Function} props.setAttributes Function that updates individual attributes.
 *
 * @return {WPElement} Element to render.
 */
export default function Edit( props ) {

	const localTerms              = window.tainacanCollectionItemsBlock.localTerms ?? [];
	const tainacanCollectionItems = window.tainacanCollectionItemsBlock.tainacanCollectionItems ?? '';
	const tainacanMetadataLocal   = window.tainacanCollectionItemsBlock.tainacanMetadataLocal ?? '';
	const i18n                    = window.tainacanCollectionItemsBlock.i18n ?? [];

	const {
		attributes,
		setAttributes,
	} = props;

	const {
		blockTitle,
		blockSubTitle,
		blockDescription,
	} = attributes;

	const blockProps = useBlockProps();

	const transitionFilter = (term) => {
		return ''
	}

	return (
		<div className="hacklab-tainacan-collection-items on-editor">
			<div className="container">
				<div className="column">
					<RichText
						tagName="h2"
						value={ blockTitle }
						allowedFormats={ [] }
						onChange={ ( value ) => setAttributes( { blockTitle: value } ) }
						placeholder={ i18n.title }
					/>

					<RichText
						tagName="h4"
						value={ blockSubTitle }
						allowedFormats={ [] }
						onChange={ ( value ) => setAttributes( { blockSubTitle: value } ) }
						placeholder={ i18n.subtitle }
					/>

					<Terms transitionFilter={ transitionFilter } localTerms={ localTerms } />

					<RichText
						tagName="p"
						value={ blockDescription }
						allowedFormats={ [] }
						onChange={ ( value ) => setAttributes( { blockDescription: value } ) }
						placeholder={ i18n.description }
					/>

					<Button className="button" href="/unidades">{ i18n.allUnits }</Button>
				</div>
				<div className="column">
					<Gallery tainacanCollectionItems={ tainacanCollectionItems } tainacanMetadataLocal={ tainacanMetadataLocal } />
					<Button className="button" href="/unidades">{ i18n.allUnits }</Button>
				</div>
			</div>
		</div>
	);
}
