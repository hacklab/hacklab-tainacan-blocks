<?php
/**
 * Plugin Name:       Hacklab Tainacan Blocks
 * Description:       Blocos para utilização em diversos projetos.
 * Version:           0.1.5
 * Requires at least: 5.8
 * Requires PHP:      7.0
 * Author:            The WordPress Contributors
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       hacklab-tainacan-blocks
 * Domain Path:       /languages
 *
 * @package           create-block
 */
define( 'HACKLAB_TAINACAN_BLOCKS_PATH', plugin_dir_path( __FILE__ ) );
define( 'HACKLAB_TAINACAN_BLOCKS_PATH_URL', plugin_dir_url( __FILE__ ) );

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/reference/functions/register_block_type/
 */

function hacklab_tainacan_blocks_init() {
	if ( ! function_exists( 'register_block_type' ) ) {
        // Block editor is not available.
        return;
    }

	$plugin_folder = HACKLAB_TAINACAN_BLOCKS_PATH;
	$blocos_ativos = [];

	if ( is_plugin_active( 'tainacan/tainacan.php' ) ) {
		$blocos_ativos = [
			'sample-block' => null,
			'tainacan-collection-items' => [
				'render_callback' => 'hacklabTainacanBlocks\\tainacan_collection_items_callback'
			]
		];
	}

	$blocos_ativos = apply_filters( 'hacklab_tainacan_blocos_ativos', $blocos_ativos );

	foreach ( $blocos_ativos as $block_name => $block_args ) {
		$args = [];
		if ( $block_args ) {
			include $plugin_folder . '/blocks/' .  $block_name . '/' . $block_name . '.php';
			foreach ( $block_args as $arg => $value ) {
				$args[$arg] = $value;
			}
		}
		register_block_type( $plugin_folder . 'blocks/' .  $block_name . '/' , $args );
	}
}

add_action( 'init', 'hacklab_tainacan_blocks_init' );

/**
 * Load plugin textdomain `hacklab-tainacan-blocks`
 */
function hacklab_tainacan_blocks_load_plugin_textdomain() {
    $path = dirname( plugin_basename( __FILE__ ) ) . '/languages';
    load_plugin_textdomain( dirname( plugin_basename( __FILE__ ) ), false, $path );
}

add_action( 'init', 'hacklab_tainacan_blocks_load_plugin_textdomain' );
