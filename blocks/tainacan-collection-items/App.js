import React, { useState } from "react";
import { Button } from '@wordpress/components';

import Title from './src/components/Title';
import SubTitle from './src/components/SubTitle';
import Description from './src/components/Description';
import Terms from './src/components/Terms';
import Gallery from './src/components/Gallery';

export function App( props ) {

	// Get terms of the taxonomy Local
	const localTerms = window.tainacanCollectionItemsBlock.localTerms ?? [];

	// ID of the metadata
	const tainacanMetadataLocal = window.tainacanCollectionItemsBlock.tainacanMetadataLocal ?? '';

	// i18n strings
	const i18n = window.tainacanCollectionItemsBlock.i18n ?? [];

	const blockTitle = props['data-building'].blockTitle
	const blockSubTitle = props['data-building'].blockSubTitle
	const blockDescription = props['data-building'].blockDescription

	const [filter, setFilter] = useState([])

	const transitionFilter = (term) => {
		const filterArray = [...filter]
		const index = filterArray.findIndex(obj => obj.id === term.id)

		if (index !== -1) {
			filterArray.splice(index, 1)
		} else {
			filterArray.push(term)
		}

		setFilter(filterArray)
	}

	const removeFilter = (term) => {
		const filterArray = [...filter]

		const index = filterArray.findIndex(obj => obj.id === term.id)

		if (index !== -1) {
			filterArray.splice(index, 1)
		}

		setFilter(filterArray)
	}

	return (
		<div>
			<div className="container">
				<div className="column">
					<Title title={ blockTitle } />
					<SubTitle subTitle={ blockSubTitle } />
					<Terms transitionFilter={ transitionFilter } localTerms={ localTerms } />
					<Description description={ blockDescription } />
					<Button className="button" href="/unidades">{ i18n.allUnits }</Button>
				</div>
				<div className="column">
					<Gallery filter={ filter } removeFilter={ removeFilter } tainacanMetadataLocal={ tainacanMetadataLocal } />
					<Button className="button" href="/unidades">{ i18n.allUnits }</Button>
				</div>
			</div>
		</div>
	);
}
