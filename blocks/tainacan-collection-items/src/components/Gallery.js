import apiFetch from '@wordpress/api-fetch';
import { React, useState, useEffect, useCallback } from 'react';
import removeIcon from "../../assets/icons/close.svg";

export default function Gallery({ filter, removeFilter, tainacanMetadataLocal, orderBy = "title" }) {

	const i18n = window.tainacanCollectionItemsBlock.i18n ?? [];

	const [getItems, setGetItems] = useState([]);
	const [loading, setLoading] = useState(true);

	const placeholderImage = 'https://via.placeholder.com/900x1100';

	// Set the placeholder image on error
	const onImageError = (e) => {
		e.target.src = placeholderImage
	}

	const factoryUrl = (metadata, filter = []) => {
		const baseUrl = "/tainacan/v2/items";
		const metakey = `metaquery[0][key]=${metadata}`
		const params = filter.map((f, index) => {
			return `metaquery[0][value][${index}]=${f.relationship}`
		})

		const perpage = "12";

		return `${baseUrl}?${metakey}&${params.join("&")}&perpage=${perpage}`;
	}

	const factoryThumbs = async (items) => {
		const itemsMapped = items.map(({ _thumbnail_id: image_id }) => {
			if (!image_id) {
				return new Promise((resolve) => {
					resolve({ without_thumb: true })
				})
			}

			return apiFetch({ path: `/wp/v2/media/${image_id}` })
		});

		const thumbnails = await Promise.all(itemsMapped);

		return thumbnails;
	}

	const factoryPostWithThumb = (post, thumbnail) => {
		return { ...post, thumbnail }
	}

	const factoryOrderPosts = useCallback((posts) => {
		return posts.sort((a, b) => {
			if (typeof a[orderBy] === "string") {
				// sort by name
				const nameA = a[orderBy].toUpperCase(); // ignore upper and lowercase
				const nameB = b[orderBy].toUpperCase(); // ignore upper and lowercase
				if (nameA < nameB) {
					return -1;
				}
				if (nameA > nameB) {
					return 1;
				}
				return 0;
			}

			return a[orderBy] - b[orderBy];
		});
	}, [orderBy])

	const fetchGallery = useCallback(async () => {

		const { items } = await apiFetch({ path: factoryUrl(tainacanMetadataLocal, filter) })

		const thumbnails = await factoryThumbs(items)

		const postsWithoutThumb = items.map((post) => {
			if (post._thumbnail_id) return null;

			return factoryPostWithThumb(post, placeholderImage)
		}).filter(e => e)

		const postsWithThumb = thumbnails.map((thumb) => {
			if (thumb.without_thumb) return null;
			const postFinded = items.find((post) => Number(post._thumbnail_id) === thumb.id)

			return factoryPostWithThumb(postFinded, thumb.source_url)
		}).filter(e => e)

		setGetItems((prevState) => [...factoryOrderPosts([...postsWithThumb, ...postsWithoutThumb])])
		setLoading(false)

	}, [factoryOrderPosts, filter])

	useEffect(() => {
		fetchGallery()
	}, [fetchGallery])

	return (
		<>
			<div className='status-term'>
				{ filter &&
					filter.map((f, index) => (
					<span key={index}>{f.title}<span relationship={ f.relationship } className='clear' onClick={ () => removeFilter(f) }><img src={ removeIcon } alt={ i18n.remove } /></span></span>
				))}
			</div>

			{ getItems && getItems.length ? (
				<div className='gallery-masonry'>
					{ getItems.map((post, index) => (
						<div className={`item item-${index}`} key={ post.id } id={`item-id-${post.id}`}>
							<h2 className='title'>{ post.title }</h2>
							<img src={ post.thumbnail } alt={ post.title } onError={ onImageError } />
						</div>
					))}
				</div>
			) : null }

			{ ! loading && ! getItems.length ? <div className='gallery-masonry no-items'><p>{ i18n.noGallery }</p></div> : null }
		</>
	)
}
