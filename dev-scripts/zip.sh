#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CDIR=$( pwd )
cd $DIR/../
zip -r zip/hacklab-tainacan-blocks.zip ./ -x "./node_modules/*" -x "./dev-scripts/*" -x "./zip/*" -x "./.*"
