import apiFetch from '@wordpress/api-fetch';
import { React, useState, useEffect, useCallback } from 'react';

export default function Terms({transitionFilter, localTerms}) {

	// i18n strings
	const i18n = window.tainacanCollectionItemsBlock.i18n ?? [];

	const [filterOptions, setFilterOptions] = useState([]);
	const [selectedTermId, setSelectedTermId] = useState(0);
	const [loading, setLoading] = useState(true);

	const handleOpen = (e) => {
		if (e.target.classList.contains('term-button')) {
			if (selectedTermId == e.target.id) {
				setSelectedTermId(0)
			} else {
				setSelectedTermId(e.target.id)
			}
		} else {
			setSelectedTermId(0)
		}
	};

	// Listener click out of the .term-button
	const handleClickOut = (e) => {
		if (!e.target.classList.contains('term-button')) {
			setSelectedTermId(0)
		}
	};

	// Listener clicks on document
	useEffect(() => {
		document.addEventListener('click', handleClickOut);

		return () => {
			document.removeEventListener('click', handleClickOut);
		};
	}, []);

	// Factory the array with terms and posts by local
	const factoryLocalTerms = useCallback((terms) => {

		const items = []

		for (var termId in terms) {
			const formatItem = {
				"id": termId,
				"name": terms[termId],
				"child": []
			}

			apiFetch({path: `wp/v2/unidade?local=${termId}&per_page=99`}).then((post) => {
				post.map(p => {
					const formatArray = {
						"id": p.id,
						"title": p.title.rendered,
						"relationship": p.relationship
					}
					formatItem['child'].push(formatArray)
				})
				setLoading(false)
			} )

			items.push(formatItem)
		}

		return items
	}, [])

	useEffect(() => {
		setFilterOptions( factoryLocalTerms(localTerms) )
	}, [])

	// Component SelectTerm
	function SelectTerm({ term, label, child }) {
		return (
			<button disabled={loading} id={term} className={(selectedTermId == term) ? 'active button term-button' : 'button term-button'} onClick={handleOpen}>
				{ loading ? i18n.loading : label }
				<div className={(selectedTermId == term) ? 'active submenu' : 'submenu'}>
					{ child && child.length ? (
						<ul>
							{child.map((c) => (
								<li key={c.id} id={c.id} relationship={c.relationship} onClick={() => transitionFilter(c)}>{c.title}</li>
							))}
						</ul>
					) : null }
				</div>
			</button>
		);
	}

	// Render the component
	return (
		<>
			{filterOptions &&
				<div className='terms'>
					{ loading ?
						i18n.loading :
						filterOptions.map((term, index) => (
							<SelectTerm key={index} term={term.id} label={term.name} child={term.child} />
						))
					}
				</div>
			}
		</>
	)
}
