import React, { Component } from 'react';

export default class Description extends Component {

	render() {
		return(
			<>
				{ this.props.description && <p className="description">{this.props.description}</p> }
			</>
		)
	}

}
