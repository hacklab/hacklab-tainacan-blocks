import React, { Component } from 'react';

export default class SubTitle extends Component {

	render() {
		return(
			<>
				{ this.props.subTitle && <p>{this.props.subTitle}</p> }
			</>
		)
	}

}
